package com.citi.training.assessment.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.assessment.dao.TradeDao;
import com.citi.training.assessment.model.Trade;
import com.citi.training.assessment.exceptions.TradeNotFoundException;

@Component
public class MysqlTradeDao implements TradeDao {

	@Autowired
	JdbcTemplate tpl;
	
	@Override
	public List<Trade> findAll() {
		return tpl.query("select id, stock, price, volume from trade", new TradeMapper());
	}
	
	private static final class TradeMapper implements RowMapper<Trade> {
		public Trade mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Trade(rs.getInt("id"),
							rs.getString("stock"),
							rs.getDouble("price"),
							rs.getInt("volume"));
		}
	} 

	@Override
	public int create(Trade trade) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        this.tpl.update(
            new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps =
                            connection.prepareStatement("insert into trade (id, stock, price, volume) values (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
                    ps.setInt(1, trade.getId());
                    ps.setString(2, trade.getStock());
                    ps.setDouble(3, trade.getPrice());
                    ps.setInt(4, trade.getVolume());
                    return ps;
                }
            },
            keyHolder);
        return keyHolder.getKey().intValue();
	}

	@Override
	public Trade findById(int id) {
		
        List<Trade> trades = this.tpl.query(
        		"select id, stock, price, volume from trade where id = ?",
                new Object[]{id},
                new TradeMapper()
        );
        if(trades.size() <= 0) {
        	throw new TradeNotFoundException("Trade not found");
        }			

        return trades.get(0);
	}

	@Override
	public void deleteById(int id) {
        this.tpl.update("delete from trade where id=?", id);
		
	}
	
}
