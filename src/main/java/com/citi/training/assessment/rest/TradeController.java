package com.citi.training.assessment.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.assessment.model.Trade;
import com.citi.training.assessment.service.TradeService;

/**
 * 
 * @author Alexandra Bagarra
 *<h1>Here we have the trade controller</h1>
 *
 *
 */

@RestController
@RequestMapping(TradeController.BASE_PATH)
public class TradeController {
    public final static String BASE_PATH = "/trade";
	@Autowired
	TradeService tradeService;
	
	
	@RequestMapping(value="/",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Trade>findAll() {
		return tradeService.findAll();
	}
	
	@RequestMapping(value="/", method=RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE)
	public HttpEntity<Trade> create(@RequestBody Trade trade) {
		tradeService.create(trade);
		return new ResponseEntity<Trade>(trade, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public Trade findById(@PathVariable int id) {
		return tradeService.findById(id);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	@ResponseBody public void deleteById(@PathVariable int id) {
		tradeService.deleteById(id);
	}
	
}
