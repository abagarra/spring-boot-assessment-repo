package com.citi.training.assessment.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.assessment.dao.TradeDao;
import com.citi.training.assessment.model.Trade;


@RunWith(SpringRunner.class)
@SpringBootTest
public class TradeServiceTests {
	@Autowired
	private TradeService tradeService;
	
	@MockBean
	private TradeDao mockTradeDao;
	
	@Test
	public void test_createRuns() {
		
		Trade testTrade = new Trade(2, "Stock", 9.99, 10);
		
		int newId = 1;
		when(mockTradeDao.create(any(Trade.class))).thenReturn(newId);
		int createdId = tradeService.create(testTrade);
		
		verify(mockTradeDao).create(testTrade);
		assertEquals(newId,createdId);
	}
	
	@Test
	public void test_deleteRuns() {
		int existingId = 1;
		
		tradeService.deleteById(existingId);
		verify(mockTradeDao).deleteById(existingId);
	}
	
	
	@Test
	public void test_findAll() { 
		List<Trade> testList = new ArrayList<Trade>();
		testList.add(new Trade(33, "LastTest", 99.9, 8));
		
		when(mockTradeDao.findAll()).thenReturn(testList);
		
		List<Trade> returnedList = tradeService.findAll();
		
		verify(mockTradeDao).findAll();
		assertEquals(testList, returnedList);
	
	
	}
	
}
