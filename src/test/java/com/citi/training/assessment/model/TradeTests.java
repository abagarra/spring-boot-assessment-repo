package com.citi.training.assessment.model;

import org.junit.Test;

import com.citi.training.assessment.model.Trade;

public class TradeTests {

	@Test
	public void test_Trade_fullConstructor() {
		String google = new String("GOOGL");
		String microsoft = new String("MSFT");
		
		Trade trade1 = new Trade(5, google, 10.01, 2);
		assert(trade1.getId() == 5);
		assert(trade1.getStock().equals(google));
		assert(trade1.getPrice() == 10.01);
		assert(trade1.getVolume() == 2);
		
		Trade trade2 = new Trade(11, microsoft, 9.59, 5);
		assert(trade2.getId() == 11);
		assert(trade2.getStock().equals(microsoft));		
		assert(trade2.getPrice() == 9.59);
		assert(trade2.getVolume() == 5);		
	}
	
	
	
}
